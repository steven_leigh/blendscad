import bpy
import mathutils

def selectAll():
    for obj in bpy.data.objects:
        obj.select = True

def selectNone():
    for obj in bpy.data.objects:
        obj.select = False
        
def selectOnly(objSel):
    selectNone()
    objSel.select = True

def deleteAll():
    selectAll()
    bpy.ops.object.delete()


class Object:
    def __init__(self):
        selectNone()
        bpy.context.scene.cursor_location = (0.0, 0.0, 0.0)
    def move(self, x, y, z):
        self.obj.location = self.obj.location + mathutils.Vector((x, y, z))
        return self
    def scale(self, x, y, z):
        s = self.obj.scale
        self.obj.scale = (x + s[0], y + s[1], z + s[2])
        return self
    def rot(self, x, y, z):
        r = self.obj.rotation_euler
        xRad = x / 360.0 * (2*3.14159)
        yRad = y / 360.0 * (2*3.14159) 
        zRad = z / 360.0 * (2*3.14159) 
        self.obj.rotation_euler = (xRad + r[0], yRad + r[1], zRad + r[2])
        return self
    def diff(self, otherObj):
        mod = self.obj.modifiers.new("ModifierName", type='BOOLEAN')
        mod.operation = 'DIFFERENCE'
        mod.object = otherObj.getObj()
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier=mod.name)
        otherObj.getObj().hide = True
        return self
    def union(self, otherObj):
        mod = self.obj.modifiers.new("ModifierName", type='BOOLEAN')
        mod.operation = 'UNION'
        mod.object = otherObj.getObj()
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier=mod.name)
        otherObj.getObj().hide = True
        return self
    def resize(self, x, y, z):
        selectNone()
        self.obj.select = True
        bpy.context.scene.objects.active = self.obj
        bpy.ops.transform.resize(value=(x,y,z))
        return self
    def getObj(self):
        return self.obj
    def setObj(self, obj):
        self.obj = obj
    def copy(self):
        selectOnly(self.obj)
        bpy.ops.object.duplicate()
        other = Object()
        other.setObj(bpy.context.active_object)
        return other
    def initMaterial(self):
        material = bpy.data.materials.new(name="MaterialName")
        material.use_transparency = True
        self.obj.data.materials.append(material)
    def color(self, r, g, b):
        selectOnly(self.obj)
        self.obj.data.materials[0].diffuse_color = (r, g, b)
        #self.obj.data.materials.append(mat)
        return self
    def alpha(self, a):
        selectOnly(self.obj)
        self.obj.data.materials[0].alpha = a;
        self.obj.show_transparent = True
        return self


class Cube(Object):
    def __init__(self, x=1, y=1, z=1):
        bpy.ops.mesh.primitive_cube_add()
        self.obj = bpy.context.active_object
        self.initMaterial()
        self.resize(x,y,z)
        
class Cylinder(Object):
    def __init__(self, r=1, h=1, vertices=64):
        bpy.ops.mesh.primitive_cylinder_add(vertices=vertices)
        self.obj = bpy.context.active_object
        self.initMaterial()
        self.resize(h,h,r)

class Sphere(Object):
    def __init__(self, r=1, subdivisions=3):
        bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=subdivisions)
        self.obj = bpy.context.active_object
        self.initMaterial()
        self.resize(r,r,r)
        
class Cone(Object):
    def __init__(self, r=1, h=1, vertices=64):
        bpy.ops.mesh.primitive_cone_add(vertices=vertices)
        self.obj = bpy.context.active_object
        self.initMaterial()
        self.resize(h,h,r)


#deleteAll()

cube = Cube()
cube.color(1,0,0)
cube.move(0,0,0)
cube.scale(0,0,0)
cube.rot(30,10,0)

cube2 = Cube()
cube2.move(0,-2,0)

cube2.diff(cube)

cube3 = Cube()
cube3.move(-0.5, -0.7, 0.5)
cube3.union(cube2)

cyl = Cylinder().color(0,1,0)
cyl.scale(2,2,2)
cyl.resize(1,1,1)
cyl.resize(1,1,1)
cyl.resize(1,1,1)
cube3.resize(2,2,2)

sphere = Sphere(4,4).move(1,6,1)

cyl.diff(sphere)

cone = Cone(r=7)

cone.getObj().select=True


cone2 = cone.copy()

cone2.move(0,0,5)
cone.color(1,1,0).move(1,3,1)

cyl.diff(cone2.move(0,0,-3))

cyl.alpha(0.7)
